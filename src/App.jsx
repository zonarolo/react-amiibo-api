import React, { useState } from 'react';
import './App.scss';
import {Amiibos} from "./pages/Amiibos/Amiibos";
import {BrowserRouter as Router, Link, Route, Switch} from "react-router-dom";
import { GameSeries } from './pages/GameSeries/GameSeries';
import { AmiibosContext } from './shared/contexts/AmiiboContext';
import {ButtonStyle} from './shared/styles/Button.style';
import { Menu } from './shared/components/Menu/Menu';
import { Home } from './pages/Home/Home';
import { Contact } from './pages/Contact/Contact';
import { AmiiboDetail } from './pages/Amiibos/AmiibosDetail/AmiibosDetail';


function App() {
  const [amiibos, setAmiibos] = useState([]);

  return (

    <Router>

        <div className="container-fluid justify-content-center my-4 u-font-size-16">
          <AmiibosContext.Provider value={[amiibos, setAmiibos]}>
            <ButtonStyle>Normal Button</ButtonStyle>
            <Menu/>
            <Switch>
              <Route path="/amiibos/:tail">
                <AmiiboDetail/>
              </Route>
              <Route path="/contact">
                <Contact/>
              </Route>
              <Route path="/amiibos">
                <Amiibos/>
              </Route>
              <Route path="/gameseries">
                <GameSeries/>
              </Route>
              <Route path="/">
                <Home/>
              </Route>
            </Switch>
            </AmiibosContext.Provider>
        </div>

    </Router>
  );
}

export default App;
