import React from 'react';

export function Home () {
    return (
        <div className="d-flex justify-content-center">
            <h1 className="b-text-primary">WELCOME TO AMIIBO REACT APP</h1>
            <img src="https://raw.githubusercontent.com/N3evin/AmiiboAPI/master/images/icon_00000000-00340102.png" alt="mario"/>
        </div>
    );
}