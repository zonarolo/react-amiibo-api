import React, { useContext } from 'react';
import { useForm } from "react-hook-form";
import { AmiibosContext } from "../../shared/contexts/AmiiboContext";
import './Contact.scss';


export function Contact () {
    const [amiibos, setAmiibos] = useContext(AmiibosContext);

    const { register, handleSubmit} = useForm();

    const onSubmit = data => console.log(data);

    return (

      <div className="c-contact">
        <form action="" className="c-contact__form" onSubmit={handleSubmit(onSubmit)}>
          <label className="c-contact__label" htmlFor="name">Name</label>
          <input type="string" name="name" id="name" placeholder="Your Name" ref={register ({required: true})} className="c-contact__input"/>

          <label className="c-contact__label" htmlFor="email">Email</label>
          <input type="email" name="email" id="email" placeholder="Your Email" ref={register ({required: true})} className="c-contact__input"/>

          <label className="c-contact__label" htmlFor="favoriteAmiibo">Favorite Amiibo</label>
          <select name="favoriteAmiibo" id="favoriteAmiibo" className="c-contact__input" ref={register({required: true})}>{amiibos.map( (item) => <option value="{item.name}">{item.name}</option>)}</select>
          <input type="submit" className="c-contact__submit"/>
        </form>
      </div>
    );
}