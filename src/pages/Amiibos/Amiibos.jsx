import React, {useState, useEffect, useContext} from "react";
import axios from "axios";
import {environment} from "../../environments/environment";
import {AmiiboGallery} from "../../shared/components/AmiiboGallery/AmiiboGallery";
import {AmiibosContext} from "../../shared/contexts/AmiiboContext";
import {AmiiboSearch} from "../../shared/components/AmiiboSearch/AmiiboSearch";

export function Amiibos() {
  // const [amiibos, setAmiibos] = useState([]);  Dejamos de utilizar el state para coger el valor del contexto
  const [amiibos, setAmiibos] = useContext(AmiibosContext);
  const [filteredAmiibos, setFilteredAmiibos] = useState([]);

  useEffect(() => {
    axios.get(environment.url + "amiibo/").then(res => {
      setAmiibos(res.data.amiibo);
      setFilteredAmiibos(res.data.amiibo);
    });
  }, []);

  function amiibosSearch(filteredValues) {
    let amiiboFilter = [];
    amiiboFilter = amiibos.filter((item, index) => {
      if (filteredValues.name !== "") {
        return item.name.toLowerCase().includes(filteredValues.name.toLowerCase());
      } else if (filteredValues.amiiboSeries !== "") {
        return item.amiiboSeries.toLowerCase().includes(filteredValues.amiiboSeries.toLowerCase());
      } else if (filteredValues.character !== "") {
        return item.character.toLowerCase().includes(filteredValues.character.toLowerCase());
      } else if (filteredValues.gameSeries !== "") {
        return item.gameSeries.toLowerCase().includes(filteredValues.gameSeries.toLowerCase());
      } else if (filteredValues.typeImg !== "") {
        return item.type.toLowerCase().includes(filteredValues.typeImg.toLowerCase());
      } else {
        return true;
      }
    });
    console.log(amiiboFilter);
    setFilteredAmiibos(amiiboFilter);
  }

  return (
    <div>
      <h1 className="b-text-primary d-flex justify-content-center">Amiibos</h1>
      <AmiiboSearch onSearch={amiibosSearch}/>
      <AmiiboGallery amiibos={filteredAmiibos}/>
    </div>
  );
}