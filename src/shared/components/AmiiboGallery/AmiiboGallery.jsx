import React, { useEffect } from 'react';
import "./AmiiboGallery.scss";
import { Link } from "react-router-dom";

// Como estamos usando hooks debemos traer los valores con "props"
export function AmiiboGallery (props) {
  
  
  return (
    <div className="c-amiibo-gallery">
      {/* se colocan llaves antes de el codigo para poder usar codigo js, porque sino nos toma como string */}
      <div className="row">
        {props.amiibos.map((item, index) =>
          <div className="col-lg-4 col-md-6 col-sm-12" >
            <Link to= {'amiibos/' + item.tail} className="c-amiibo-gallery__item-container" key="{index}">
              <figure key={index} className="c-amiibo-gallery__item">
                <img className="c-amiibo-gallery__img" src={item.image} alt=""/>
                <h2 className="c-amiibo-gallery__text">{item.name}</h2>
              </figure>
            </Link>
          </div>
        )}
      </div>
    </div>
  );
}