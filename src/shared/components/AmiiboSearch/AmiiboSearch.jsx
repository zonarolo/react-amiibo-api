import React from 'react';
import './AmiiboSearch.scss';
import { useForm } from "react-hook-form";

export function AmiiboSearch (props){
  const { register, handleSubmit, watch, errors } = useForm();

  function onSubmit(data){
    props.onSearch(data)
  }

  return (
    <div className="c-amiiboSearch">
        <form onSubmit={handleSubmit(onSubmit)} className="c-amiiboSearch__form">

            <label className="c-amiiboSearch__label" htmlFor="name">Name
                <input className="c-amiiboSearch__input" id="name" name="name" ref={register}  />
            </label>

            <label className="c-amiiboSearch__label" htmlFor="amiiboSeries">Amiibo Series
                <input className="c-amiiboSearch__input" id="amiiboSeries" name="amiiboSeries"  ref={register} />
            </label>

            <label className="c-amiiboSearch__label" htmlFor="character">Character
                <input className="c-amiiboSearch__input" id="character" name="character" ref={register}  />
            </label>

            <label className="c-amiiboSearch__label" htmlFor="gameSeries">gameSeries
                <input className="c-amiiboSearch__input" id="gameSeries" name="gameSeries"  ref={register} />
            </label>

            <label className="c-amiiboSearch__label" htmlFor="AmiiboFavorito">Amiibo Favorito

                <select className="c-amiiboSearch__input" name="typeImg" ref={register} >
                    <option>Figure</option>
                    <option>Card</option>
                </select>

            </label>


            {errors.exampleRequired && <span>This field is required</span>}

            <input className="c-amiiboSearch__submit" type="submit" />

        </form>
    </div>

  )
}