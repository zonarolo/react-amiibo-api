import React, { useEffect } from 'react';
import "./TextList.scss";

// Como estamos usando hooks debemos traer los valores con "props"
export function TextList (props) {
  
  return (
    <div className="c-text-list">
      {/* se colocan llaves antes de el codigo para poder usar codigo js, porque sino nos toma como string */}
      <div className="row">
        {props.texts.map((item, index) =>
          <div className="col-lg-4 col-md-6 col-sm-12">
            <div className="c-text-list__item">
              <h4 className="c-text-list__text" key="{item.name}">{item.name}</h4>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}